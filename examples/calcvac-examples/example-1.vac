# HERA.VAC       Settings for vacline and calcvac (c) Mike Seidel + Markus Hoffmann
#
# Zum Berechnen des Drucks im HERA-e Bogen
# Letzte Bearbeitung: 10.05.2003   MH
#
[general]

#
# This is the overal HERA-Interaction Vacuum Geometry file
# 
#

TITLE="HERA-e Bogen"


[pumping]
# specific pumpung speeds (l/s/m) or
# relative pumping speeds compared to nitrogen-equivalent

cold: 0(default)   0(2)  20(16) 30(28)
tsp:  0(default)   1(2)   0(16)  1(28)
neg:  0(default) 1.6(2)   0(16)  1(28)
igp:  0(default)   2(2) 0.6(16)  1(28)


# Materialspezifische Ausgasraten fuer calcvac
[outgasing]
# Unit: mbar l/s /cm^2

steel:	    0(default)	1.0e-12(2)	 0(16)   5e-13(28)
wolfram:    0(default)	1.0e-12(2)	 0(16)   5e-13(28)
Cu:	    0(default)	1.0e-12(2)	 0(16)   1e-12(28)
Al:	    0(default)	2.0e-12(2)	 1e-13(16)   20e-12(28)
AlBe:	    0(default)	2.0e-12(2)	 1e-13(16)   20e-12(28)
cold:	    0(default)	1.0e-13(2)	 0(16)       0(28)
neg:	    0(default)	      0(2)   8e-14(16)       0(28)
tspheat:    0(default)	  5800e-11(2)   5.8e-11(16)   5.8e-11(28)
heating:    0(default)	  4e-11(2) 0.7e-11(16)   6e-11(28)
syli:	    0(default)	  1e-11(2) 0.8e-11(16)   8e-11(28)

###############################################################################
# lattice section. Here the lattice of all beamlines are defined
#
#  If [ZEUS] is given, the following lines are for ZEUS only
#  If [H1] is given, the following lines are for H1 only
#  If [common] is given, the following lines are for H1 and ZEUS
#
[lattice]
#
# Here all components are defined
#
# PUMP:   L,MAT,TYP,SPEED,FORM
# ROHR:   L,MAT,FORM
# TYP=cold:   T=Temperatur in K
# FORM=circ   d=durchmesser
# FORM=ellipt a,b=Durchmesser
# FORM=rect   a,b=Seitenlaengen
# FORM=schluesselloch  a,b=Durchmesser zentral, c,d=Seitenlaengen Photonenrohrauswuchs

Begin:   START, S0=0

# This will be used to separate Vacuum systems

VENTIL: SECTION, L=0.03, q=0, W=0.0001, A=3100, BOUNDARY(Q=0)

ks1: ROHR, L=0.600, mat=kupfer, form=ellipt, a=0.080, b=0.04
ks2: ROHR, L=1.100, mat=kupfer, form=ellipt, a=0.080, b=0.04
QS1: ROHR, L=1.500, mat=kupfer, form=ellipt, a=0.080, b=0.04
QS2: ROHR, L=2.000, mat=kupfer, form=ellipt, a=0.080, b=0.04
DIP_A:ROHR, L=0.0925, mat=kupfer, form=ellipt, a=0.080, b=0.04
DIP_P:PUMP, L=9.000,  mat=kupfer, form=ellipt, a=0.080, b=0.04, typ=neg, speed=495
DIP: LINE=(DIP_A,DIP_P,DIP_A)

VARIAN: PUMP, L=0.1, mat=steel, form=ellipt, a=0.080, b=0.040, typ=igp, speed=60

End:	 END

LINK, Begin, VENTIL

[ZEUS]
ZEUSline: LINE= (Begin,KS1,QS1,DIP,KS2,QS2,VARIAN,DIP,VENTIL,End)

#######################################################################
[events]
#
# Hier werden die Events zugeordnet. Z=ZEUS, H=H1
# action darf sein: none,heat,syli,off
# In Klammern die Staerke des Ausheitzens oder der Sylibeleuchtung
#
Z0:  action=none,        comment="Normalzustand"
Z1:  action=heat(1.8),   comment="TSP geheizt"

[plot]
# Plot kommandos (passed to gnuplot)
set grid
set xrange [0:23]

[measurements]
[ZEUS]
% Pos     H0        H1      H2      H3  Event 3    Event 5	Base 2
 -5.8   6.91e-11 6.91e-11 6.91e-11 2.84e-9  6.91e-11   8.1e-10	6.91e-11
 
