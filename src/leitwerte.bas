' Berechnet Leitwerte fuer verschiedene Vakumkammergeometrien
' (c) Markus Hoffmann 2003         interpreter: X11-Basic
'
'

PRINT "Schluessellochkammer: leitwert=";@leitwert_schluesselloch(0.090,0.062,0.110,0.018,1,28)
PRINT "Schluessellochkammer: umfang=";@umfang_schluesselloch(0.090,0.062,0.110,0.018)
PRINT "Elliptische Kammer: Leitwert=";@leitwert_ellipt(0.090,0.060,1,28)
PRINT "Elliptische Kammer: Umfang=";@umfang_ellipt(0.090,0.060)
PRINT "Rechteckige Kammer: leitwert=";@leitwert_rechteck(0.110,0.018,1,28)
QUIT

FUNCTION leitwert_schluesselloch(a,b,c,d,l,m)
  RETURN @leitwert_ellipt(a,b,l,m)+@leitwert_rechteck(c,d,l,m)
ENDFUNC
FUNCTION umfang_schluesselloch(a,b,c,d)
  RETURN 2*c*10000+@umfang_ellipt(a,b)
ENDFUNC
FUNCTION kk(x)
  RETURN 0.0653947/(x+0.0591645)+1.0386
ENDFUNC
' Leitwert fuer eine Elliptische Kammer der Laenge l fuer ein
' Gas der Molekuelmassenzahl m

FUNCTION leitwert_rund(a,l,m)
  LOCAL c1,c2,c3
  MUL a,100
  MUL l,100
  c1=11.6*a*a*pi/4
  c2=17.1*a*a*a*a/sqrt(a*a+a*a)/l
  c3=1/(1/c1+1/c2)
  RETURN int(sqrt(28/m)*c3*100)/100   ! in l/s
ENDFUNC
FUNCTION leitwert_ellipt(a,b,l,m)
  LOCAL c1,c2,c3
  a=a*100
  b=b*100
  l=l*100
  c1=11.6*a*b*pi/4
  c2=17.1*a*a*b*b/sqrt(a*a+b*b)/l
  c3=1/(1/c1+1/c2)
  RETURN int(sqrt(28/m)*c3*100)/100   ! in l/s
ENDFUNC
FUNCTION leitwert_rechteck(a,b,l,m)
  LOCAL c1,c2,c3
  MUL a,100
  MUL b,100
  MUL l,100
  c1=11.6*a*b
  c2=30.9*@kk(MIN(a/b,b/a))
  MUL c2,a*a*b*b/(a+b)/l
  c2=17.1*a*a*b*b/sqrt(a*a+b*b)/l
  c3=1/(1/c1+1/c2)
  RETURN int(sqrt(28/m)*c3*100)/100   ! in l/s
ENDFUNC

FUNCTION umfang_ellipt(ua,ub)
  LOCAL eprod,oprod,i,aa,bb,eps,sum
  IF ub>ua
    aa=ub
    ub=ua
    ua=aa
  ENDIF
  aa=ua/2
  bb=ub/2
  eprod=1
  oprod=1
  eps=(aa*aa-bb*bb)/aa/aa
  sum=1
  FOR i=1 TO 10
    MUL oprod,2*i-1
    MUL eprod,2*i
    ADD sum,-1/(2*i-1)*eps^i*(oprod/eprod)^2
  NEXT i
  ' Genauigkeit etwa 1e-4
  RETURN int(ua*pi*sum*100*100)   ! in cm^2/m
ENDFUNC
FUNCTION umfang_rechteck(a,b)
  RETURN int(2*(a+b)*100*100)   ! in cm^2/m
ENDFUNC
