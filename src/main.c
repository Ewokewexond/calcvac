/* MAIN.C 

   (c) Markus Hoffmann 2002-2011 
*/

/* This file is part of VACLINE, pressure profile calculation
 * ============================================================
 * VACLINE is scientific software and comes with NO WARRANTY - read the file
 * COPYING for details.
 */

#include <stdio.h>
#include <stdlib.h>
#ifdef WINDOWS
#define EX_OK 0
#define EX_IOERR	74	/* input/output error */
#define EX_NOINPUT	66	/* cannot open input */
#define EX_DATAERR	65	/* data format error */
#else
#include <sysexits.h>
#endif
#include <string.h>

#include "vacline.h"
#include "utility.h"

int verbose=1;                         /* Verbose level */
static const char *vacline_name="vacline";
static int loadfile=FALSE;
static char ifilename[256]="input.vac";
static char ofilename[256]="output.dat";
static char twissfilename[256]="output.twiss";
static int dotwiss=0;
static int dooutput=0;
double resolution=0.001; /* minimal resolution 1 mm */
int nplot=NPLOT;         /* maximal number of output points*/

static void intro(){
  printf("*********************************************************\n"
         "*           %10s                V." VERSION "            *\n"
         "*             by Mike Seidel 2002                       *\n"
         "*             by Markus Hoffmann 2002-2011              *\n"
         "* Copyright (c) 2002-2011 --                            *\n"
	 "*                DESY Deutsches Elektronen Synchrotron, *\n" 
         "*   Ein Forschungszentrum der Helmholtz-Gemeinschaft    *\n"
         "*********************************************************\n\n",vacline_name); 
}
 
static void usage(){
  printf("\n Usage:\n ------\n\n"
         " %s [-h] [<filename>] --- process vacuum line [%s]\n\n",vacline_name,ifilename);
  printf("-r <number>\t\t--- specify resolution [m]. default: %g m\n",resolution);
  printf("--resolution <number>\t--- specify resolution [m]. default: %g m\n",resolution);
  printf("-n <number>\t\t--- specify maximum number of output points. default: %d\n",nplot);
  printf("--nplot <number>\t--- specify maximum number of output points. default: %d\n",nplot);
  printf("-o <filename>\t\t--- place output into file       [%s]\n",ofilename);
  printf("--twiss <filename>\t--- place twiss output into file [%s]\n",twissfilename);
  puts("-h --help\t\t--- Usage\n"
       "-v\t\t\t--- be more verbose\n"
       "-q\t\t\t--- be more quiet");
}


/* process command line*/

static void kommandozeile(int anzahl, char *argumente[]) {
  int count,quitflag=0;

  for(count=1;count<anzahl;count++) {
    if(!strcmp(argumente[count],"-h") || !strcmp(argumente[count],"--help")) {
      intro();
      usage();
      quitflag=1;   
    } else if(!strcmp(argumente[count],"-r") || 
      !strcmp(argumente[count],"--resolution")) {
      resolution=atof(argumente[++count]);
    } else if(!strcmp(argumente[count],"-n") || 
      !strcmp(argumente[count],"--nplot")) {
      nplot=atof(argumente[++count]);
    } 
    else if(!strcmp(argumente[count],"-v")) verbose++;
    else if(!strcmp(argumente[count],"-q")) verbose--;
    else if(!strcmp(argumente[count],"-o")) {
      dooutput=1;
      strcpy(ofilename,argumente[++count]);
    } else if(!strcmp(argumente[count],"--version")) {
      intro();
      puts("This program is scientific software. The used methods and formulas are\n" 
            "published in DESY-HERA-03-23: Markus Hoffmann, \n'Vakuum-Simulationsrechnung " 
            "fuer HERA'.\n\n"
	    "This program is open source and licensed under the GPLv2."
	    "This program is distributed in the hope that it will be useful, "
	    "but WITHOUT ANY WARRANTY; without even the implied warranty of "
    	    "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.");
    } else if(!strcmp(argumente[count],"--twiss")) {
      dotwiss=1;
      strcpy(twissfilename,argumente[++count]);
    } else if(*(argumente[count])=='-') {
      printf("calcvac: Unknown option: %s\n",argumente[count]);
    } else {
      if(!loadfile) {
        loadfile=TRUE;
        strcpy(ifilename,argumente[count]); 
      }
    }
  }
  if(quitflag) exit(EX_OK);
}

/*   main   */

int main(int anzahl, char *argumente[]) {
  char dummy[256];
  if(anzahl<2) {    /* Kommandomodus */
    intro();
    return(EX_OK);
  }
  kommandozeile(anzahl, argumente);    /* Kommandozeile bearbeiten */
  if(loadfile) {
    if(exist(ifilename)) {
      if(verbose) printf("PASS 0\n");
      Load_VacLine(ifilename);
      if(verbose) printf("PASS 1\n");
      Eval_Symbols(0);
      solve_lines();
      if(verbose) printf("PASS 2\n");
      Build_Vacuum(); //v1.FitP();
      if(!dooutput) {
        wort_sepr(ifilename,'.',0,ofilename,dummy);
  	strcat(ofilename,".dat");
      }
      write_pressure_data(ofilename);
      if(dotwiss) write_twiss(twissfilename); 
      
      if(verbose>1) List_Elements();
      if(verbose>2) List_Symbols();
      
    } else {
      printf("ERROR: file %s not found !\n",ifilename);
      return(EX_NOINPUT);
    }
  }
  return(EX_OK);
}
