# File for gnuplot, which plots the Phasespace for the vacuumline
# (c) markus Hoffmann 2003
#
set term x11
set title "vacuum phase space"
set size 0.7,1
set origin 0,0
set xlabel "P[mbar]"
set ylabel "gas flow [mbar l/s]"
set logscale x
set format x "%5.1e"
set key bottom
plot [][] "test.dat" u 2:3 ti "+/- 20 m" w l lt 4 lw 2, \
 "test.dat" u 2:((abs($1)<=6)?($3):1/0) ti "+/- 6 m" w l lt 2 lw 2, \
 "test.dat" u 2:((abs($1)<=3)?($3):1/0) ti "+/- 3 m" w l lt 3 lw 2, \
 "test.dat" u 2:((abs($1)<=1)?($3):1/0) ti "+/- 1 m" w l lt 1 lw 2
    pause -1
  
set term post col sol eps 22
set output "phasespace.eps"
replot
