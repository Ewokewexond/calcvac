# 
# (c) Markus Hoffmann 2003
#

set grid
set term post 
set term post enhanced 
set term post portrait
set term post color 
set term post solid 16
set out "test.ps"
set title "Vacuum profile by vacline"
set multiplot
set size 1,0.5
set origin 0,0.5
set ylabel "P[mbar]"
set logscale y
set format y "%5.1e"
#set arrow from 0,3e-11 to 0,2e-10 lt 2 lw 2
#set label "IP" at 0,2e-11
#set arrow from -5.8,1e-8 to -5.8,3e-9 nohead lt 3
#set arrow from 5.8,1e-8 to 5.8,3e-9 nohead lt 3
#set arrow from -2,1e-8 to -2,3e-9 nohead lt 1
#set arrow from 2,1e-8 to 2,3e-9 nohead lt 1
set key bottom
plot [][] "test.dat" u 1:2 ti "pressure" w l lt 1 lw 2

set notitle
set ylabel "flow [l/s]"
set size 1,0.16
set origin 0,0.33
set key top
set nolog
set nolabel
set yrange [:]
set format y "%7.7g"
plot "test.dat" u 1:3 ti "" w steps lt 2 lw 2
 
set ylabel "sps [l/s m]"
set size 1,0.16
set origin 0,0.16
set key top
set nolog
set nolabel
set yrange [0:400]
set format y "%7.7g"
plot "test.dat" u 1:6 ti "" w steps lt 3 lw 2
set ylabel "sog [l/s m]"
set xlabel "pos[m]"
set size 1,0.16
set origin 0,0
set key top
set nolog
set nolabel
set yrange [0:2e-7]
set format y "%7.7g"
plot "test.dat" u 1:5 ti "" w steps lt 3 lw 2


set nomulti
